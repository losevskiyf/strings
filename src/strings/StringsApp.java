package strings;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringsApp {

    private static String replaceEverySecondWordOOP(String target){
        Pattern pattern = Pattern.compile("Object-oriented programming", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(target);
        StringBuilder result = new StringBuilder();

        int counter = 0;

        while (matcher.find()){
            counter++;
            if(counter%2==0) matcher.appendReplacement(result, "OOP");
        }

        matcher.appendTail(result);
        return result.toString();
    }

    private static String findWordWithMinNumberOfDistinctCharacters(String target){

        String minWord = null;
        int minValue = Integer.MAX_VALUE;

        String[] words = target.split("[^0-9A-Za-z]+");
        for(String word: words){
            int numberOfDistinctCharacter = findNumberOfDistinctCharacter(word);
            if(numberOfDistinctCharacter<minValue) {
                minWord = word;
                minValue = numberOfDistinctCharacter;
            }
        }
        return minWord;
    }

    private static int findNumberOfDistinctCharacter(String word) {
        Set<Character> set = new HashSet<>();
        for (Character c: word.toCharArray())
            set.add(c);
        return set.size();
    }

    private static int findNumberOfLatinWords(String target){
        String[] words = target.split("[^a-zA-Z]+");
        if(words[0].equals(""))
            return words.length-1;
        else
            return words.length;
    }

    private static boolean isItPalindrom(String word){
        char[] chars = word.toCharArray();
        int i1 = 0;
        int i2 = chars.length - 1;
        while (i2 > i1) {
            if (chars[i1] != chars[i2])
                return false;
            ++i1;
            --i2;
        }
        return true;
    }

    private static List<String> findPalindroms(String target){
        String[] words = target.split("[^a-zA-Z_0-9\\-]+");
        List<String> palindroms = new ArrayList<>();
        for(String word: words)
            if(isItPalindrom(word)) palindroms.add(word);
        return palindroms;
    }

    public static void main(String[] args) {

        //Замените в строке каждое второе вхождение «object-oriented programming» (не учитываем регистр символов) на «OOP»
        String text = "Object-oriented programming is a programming language model organized" +
                " around objects rather than \"actions\" and data rather than logic. Object-oriented" +
                " programming blabla. Object-oriented programming bla.\" должна быть преобразована" +
                " в \"Object-oriented programming is a programming language model organized around" +
                " objects rather than \"actions\" and data rather than logic. OOP blabla. " +
                "Object-oriented programming bla.";
        System.out.println(replaceEverySecondWordOOP(text));

        //Найти слово, в котором число различных символов минимально
        System.out.println(findWordWithMinNumberOfDistinctCharacters("fffff ab f 1234 jkjk"));

        //Найти количество слов, содержащих только символы латинского алфавита
        System.out.println(findNumberOfLatinWords("fffff ab f 1234 jkjk"));

        //Найти слова палиндромы
        System.out.println(findPalindroms("fffff ab f 1234 jkjk"));
    }
}
